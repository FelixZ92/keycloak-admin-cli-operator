package utils

import (
	"fmt"
	"github.com/Nerzal/gocloak/v4"
	"os"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
)

const keycloakEndpointVar = "KEYCLOAK_ENDPOINT"
const (
	keycloakAdminUserNameVar = "KEYCLOAK_ADMIN_USER"
	keycloakAdminPasswordVar = "KEYCLOAK_ADMIN_PASSWORD"
	keycloakRealmVar         = "KEYCLOAK_REALM"
)

var log = logf.Log.WithName("utils_keycloak")

// CreateKeycloakClient creates a rest client for keycloak
func CreateKeycloakClient() (gocloak.GoCloak, error) {
	endpoint, err := GetKeycloakEndpoint()
	if err != nil {
		return nil, err
	}
	return gocloak.NewClient(endpoint), nil
}

// LoginToKeycloak fetches token from keycloak
func LoginToKeycloak(client gocloak.GoCloak) *gocloak.JWT {
	userName, err := getKeycloakAdminUserName()
	if err != nil {
		panic(err)
	}
	password, err := getKeycloakAdminPassword()
	if err != nil {
		panic(err)
	}
	realm, err := getKeycloakRealm()
	log.Info("Logging in...")
	token, err := client.LoginAdmin(userName, password, realm)
	if err != nil {
		panic(err)
	}
	return token
}

// ValidateAccessToken validates JWT
func ValidateAccessToken(client gocloak.GoCloak, jwt *gocloak.JWT) bool {
	realm, err := getKeycloakRealm()
	if err != nil {
		panic(err)
	}
	_, claims, err := client.DecodeAccessToken(jwt.AccessToken, realm)
	if err != nil {
		log.Error(err, "decoding failed")
		return false
	}
	err = claims.Valid()
	if err != nil {
		log.Info("token invalid")
		return false
	}
	return true
}

// RefreshAccessToken refreshes JWT
func RefreshAccessToken(client gocloak.GoCloak, jwt *gocloak.JWT) *gocloak.JWT {
	realm, err := getKeycloakRealm()
	if err != nil {
		panic(err)
	}
	newToken, err := client.RefreshToken(jwt.RefreshToken, "admin-cli", "", realm)
	if err != nil {
		panic(err)
	}
	return newToken
}

// GetKeycloakEndpoint exposes the keycloak url
func GetKeycloakEndpoint() (string, error) {
	keycloakEndpoint, found := os.LookupEnv(keycloakEndpointVar)
	if !found {
		return "", fmt.Errorf("%s must be set", keycloakEndpointVar)
	}
	return keycloakEndpoint, nil
}

func getKeycloakAdminUserName() (string, error) {
	keycloakAdminUserName, found := os.LookupEnv(keycloakAdminUserNameVar)
	if !found {
		return "", fmt.Errorf("%s must be set", keycloakAdminUserNameVar)
	}
	return keycloakAdminUserName, nil
}

func getKeycloakAdminPassword() (string, error) {
	keycloakAdminPassword, found := os.LookupEnv(keycloakAdminPasswordVar)
	if !found {
		return "", fmt.Errorf("%s must be set", keycloakAdminPassword)
	}
	return keycloakAdminPassword, nil
}

func getKeycloakRealm() (string, error) {
	keycloakRealm, found := os.LookupEnv(keycloakRealmVar)
	if !found {
		return "", fmt.Errorf("%s must be set", keycloakRealmVar)
	}
	return keycloakRealm, nil
}
func getOperatorName() (string, error) {
	keycloakRealm, found := os.LookupEnv(keycloakRealmVar)
	if !found {
		return "", fmt.Errorf("%s must be set", keycloakRealmVar)
	}
	return keycloakRealm, nil
}
