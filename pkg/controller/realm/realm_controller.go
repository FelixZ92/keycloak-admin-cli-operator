package realm

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/Nerzal/gocloak/v4"
	"github.com/go-logr/logr"
	"github.com/go-resty/resty/v2"
	"github.com/operator-framework/operator-sdk/pkg/k8sutil"
	"gitlab.com/felixz92/keycloak-admin-cli-operator/pkg/utils"
	"os"
	"strings"

	keycloakv1alpha1 "gitlab.com/felixz92/keycloak-admin-cli-operator/pkg/apis/keycloak/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("controller_realm")

const keycloakRealmFinalizer = "finalizer.realms.keycloak.zippelf.com"

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new Realm Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	keycloakClient, err := utils.CreateKeycloakClient()
	if err != nil {
		log.Error(err, "Error creating keycloak-client")
	}
	keycloakToken := utils.LoginToKeycloak(keycloakClient)
	return &ReconcileRealm{client: mgr.GetClient(), scheme: mgr.GetScheme(), keycloakClient: keycloakClient, keycloakToken: keycloakToken, restClient: resty.New()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("realm-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource Realm
	err = c.Watch(&source.Kind{Type: &keycloakv1alpha1.Realm{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	// external secondary resource (keycloak)

	return nil
}

// blank assignment to verify that ReconcileRealm implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileRealm{}

// ReconcileRealm reconciles a Realm object
type ReconcileRealm struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client         client.Client
	scheme         *runtime.Scheme
	keycloakClient gocloak.GoCloak
	keycloakToken  *gocloak.JWT
	// gocloak does not support to update a realm, so we need to talk to keycloak directly
	restClient *resty.Client
}

// Reconcile reads that state of the cluster for a Realm object and makes changes based on the state read
// and what is in the Realm.Spec
// Note:
// The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileRealm) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := log.WithValues("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Info("Reconciling Realm")

	isKeycloakTokenValid := utils.ValidateAccessToken(r.keycloakClient, r.keycloakToken)
	if !isKeycloakTokenValid {
		newToken := utils.RefreshAccessToken(r.keycloakClient, r.keycloakToken)
		if newToken != nil {
			r.keycloakToken = newToken
			reqLogger.Info("Token expired, requeue")
			return reconcile.Result{Requeue: true}, nil
		}
	}
	// Fetch the Realm instance
	instance := &keycloakv1alpha1.Realm{}
	err := r.client.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	isRealmMarkedToBeDeleted := instance.GetDeletionTimestamp() != nil
	if isRealmMarkedToBeDeleted {
		return r.finalize(instance, reqLogger)
	}

	// Add finalizer for this CR
	if !utils.Contains(instance.GetFinalizers(), keycloakRealmFinalizer) {
		if err := r.addFinalizer(reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}
	}

	// Read the json data
	realmData := gocloak.RealmRepresentation{}
	err = json.Unmarshal([]byte(instance.Spec.Data), &realmData)
	if err != nil {
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	// Fetch the client resource from keycloak
	liveRealm, err := r.keycloakClient.GetRealm(r.keycloakToken.AccessToken, *realmData.ID)
	if err != nil {
		// Error reading the object - requeue the request.
		if !strings.Contains(err.Error(), "404") {
			// Error reading the object - requeue the request.
			return reconcile.Result{}, err
		}
	}

	operatorName, found := os.LookupEnv(k8sutil.OperatorNameEnvVar)
	if !found {
		return reconcile.Result{}, fmt.Errorf("%s must be set", k8sutil.OperatorNameEnvVar)
	}

	if liveRealm != nil && instance.Spec.Data != instance.Status.Data {
		// Update it!
		reqLogger.Info("Update realm")
		realmData.Attributes["managed-by"] = operatorName

		endpoint, err := utils.GetKeycloakEndpoint()
		_, err = r.restClient.R().EnableTrace().
			SetAuthToken(r.keycloakToken.AccessToken).
			SetHeader("Accept", "application/json").
			SetHeader("Content-Type", "application/json").
			SetPathParams(map[string]string{
				"realm": *realmData.ID,
			}).
			SetBody(instance.Spec.Data).
			Put(endpoint + "auth/admin/realms/{realm}")
		if err != nil {
			return reconcile.Result{}, fmt.Errorf("could not update realm %s", instance.Name)
		}

		instance.Status.RealmID = *liveRealm.ID
		instance.Status.Data = instance.Spec.Data
		err = r.client.Status().Update(context.TODO(), instance)
		if err != nil {
			reqLogger.Error(err, "failed to update realm")
			return reconcile.Result{}, err
		}
	} else if liveRealm == nil {
		// create it!
		reqLogger.Info("Create realm")
		realmData.Attributes["managed-by"] = operatorName
		realmID, err := r.keycloakClient.CreateRealm(r.keycloakToken.AccessToken, realmData)
		if err != nil {
			reqLogger.Error(err, "failed to create realm")
			return reconcile.Result{}, err
		}

		instance.Status.RealmID = realmID
		instance.Status.Data = instance.Spec.Data
		err = r.client.Status().Update(context.TODO(), instance)
		if err != nil {
			reqLogger.Error(err, "failed to update Realm")
			return reconcile.Result{}, err
		}
	} else {
		reqLogger.Info("not changed")
	}
	return reconcile.Result{}, nil
}

func (r *ReconcileRealm) finalize(instance *keycloakv1alpha1.Realm, reqLogger logr.Logger) (reconcile.Result, error) {
	if utils.Contains(instance.GetFinalizers(), keycloakRealmFinalizer) {
		// Run finalization logic for finalizer. If the
		// finalization logic fails, don't remove the finalizer so
		// that we can retry during the next reconciliation.
		if err := r.finalizeRealm(reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}

		// Remove finalizer. Once all finalizers have been
		// removed, the object will be deleted.
		instance.SetFinalizers(utils.Remove(instance.GetFinalizers(), keycloakRealmFinalizer))
		err := r.client.Update(context.TODO(), instance)
		if err != nil {
			return reconcile.Result{}, err
		}
	}
	return reconcile.Result{}, nil
}

func (r *ReconcileRealm) finalizeRealm(reqLogger logr.Logger, realm *keycloakv1alpha1.Realm) error {
	err := r.deleteProtocolMappers(reqLogger, realm)
	err = r.deleteClientScopes(reqLogger, realm)
	err = r.deleteKeycloakClients(reqLogger, realm)
	err = r.deleteUsers(reqLogger, realm)
	err = r.deleteUserGroups(reqLogger, realm)

	err = r.keycloakClient.DeleteRealm(r.keycloakToken.AccessToken, realm.Status.RealmID)
	if err != nil {
		if !strings.Contains(err.Error(), "404") {
			// Error reading the object - requeue the request.
			return err
		}
		reqLogger.Error(err, "client not found")
	}

	//TODO: delete all clients, protocol mappers and client scopes associated with this realm

	reqLogger.Info("Successfully finalized Realm")
	return nil
}

func (r *ReconcileRealm) deleteProtocolMappers(reqLogger logr.Logger, realm *keycloakv1alpha1.Realm) error {
	mappers := &keycloakv1alpha1.ProtocolMapperList{}
	err := r.client.List(context.TODO(), mappers)
	if err != nil {
		reqLogger.Error(err, "delete realm - failed to get ProtocolMapper")
	}
	filter := func(mapper keycloakv1alpha1.ProtocolMapper) bool { return mapper.Spec.Realm == realm.Status.RealmID }
	relevantMappers := filterProtocolMapper(mappers.Items, filter)
	for _, mapper := range relevantMappers {
		err = r.client.Delete(context.TODO(), &mapper)
		if err != nil {
			reqLogger.Error(err, "failed to delete ProtocolMapper")
		}
	}
	return err
}

func (r *ReconcileRealm) deleteClientScopes(reqLogger logr.Logger, realm *keycloakv1alpha1.Realm) error {
	scopes := &keycloakv1alpha1.ClientScopeList{}
	err := r.client.List(context.TODO(), scopes)
	if err != nil {
		reqLogger.Error(err, "delete realm - failed to get ClientScope")
	}
	filter := func(mapper keycloakv1alpha1.ClientScope) bool { return mapper.Spec.Realm == realm.Status.RealmID }
	relevantScopes := filterClientScopes(scopes.Items, filter)
	for _, scope := range relevantScopes {
		err = r.client.Delete(context.TODO(), &scope)
		if err != nil {
			reqLogger.Error(err, "failed to delete ClientScope")
		}
	}
	return err
}

func (r *ReconcileRealm) deleteKeycloakClients(reqLogger logr.Logger, realm *keycloakv1alpha1.Realm) error {
	clients := &keycloakv1alpha1.KeycloakClientList{}
	err := r.client.List(context.TODO(), clients)
	if err != nil {
		reqLogger.Error(err, "delete realm - failed to get KeycloakClient")
	}
	filter := func(mapper keycloakv1alpha1.KeycloakClient) bool { return mapper.Spec.Realm == realm.Status.RealmID }
	relevantClients := filterClients(clients.Items, filter)
	for _, kc := range relevantClients {
		err = r.client.Delete(context.TODO(), &kc)
		if err != nil {
			reqLogger.Error(err, "failed to delete KeycloakClient")
		}
	}
	return err
}

func (r *ReconcileRealm) deleteUsers(reqLogger logr.Logger, realm *keycloakv1alpha1.Realm) error {
	users := &keycloakv1alpha1.UserList{}
	err := r.client.List(context.TODO(), users)
	if err != nil {
		reqLogger.Error(err, "delete realm - failed to get Users")
	}
	filter := func(mapper keycloakv1alpha1.User) bool { return mapper.Spec.Realm == realm.Status.RealmID }
	relevantUsers := filterUsers(users.Items, filter)
	for _, kc := range relevantUsers {
		err = r.client.Delete(context.TODO(), &kc)
		if err != nil {
			reqLogger.Error(err, "failed to delete User")
		}
	}
	return err
}

func (r *ReconcileRealm) deleteUserGroups(reqLogger logr.Logger, realm *keycloakv1alpha1.Realm) error {
	userGroups := &keycloakv1alpha1.UserGroupList{}
	err := r.client.List(context.TODO(), userGroups)
	if err != nil {
		reqLogger.Error(err, "delete realm - failed to get UserGroups")
	}
	filter := func(mapper keycloakv1alpha1.UserGroup) bool { return mapper.Spec.Realm == realm.Status.RealmID }
	relevantUserGroups := filterUserGroups(userGroups.Items, filter)
	for _, kc := range relevantUserGroups {
		err = r.client.Delete(context.TODO(), &kc)
		if err != nil {
			reqLogger.Error(err, "failed to delete UserGroups")
		}
	}
	return err
}

func (r *ReconcileRealm) addFinalizer(reqLogger logr.Logger, realm *keycloakv1alpha1.Realm) error {
	reqLogger.Info("Adding Finalizer for the Realm")
	realm.SetFinalizers(append(realm.GetFinalizers(), keycloakRealmFinalizer))

	// Update CR
	err := r.client.Update(context.TODO(), realm)
	if err != nil {
		reqLogger.Error(err, "Failed to update Realm with finalizer")
		return err
	}
	return nil
}

func filterProtocolMapper(mappers []keycloakv1alpha1.ProtocolMapper, test func(keycloakv1alpha1.ProtocolMapper) bool) (ret []keycloakv1alpha1.ProtocolMapper) {
	for _, s := range mappers {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return ret
}

func filterClientScopes(mappers []keycloakv1alpha1.ClientScope, test func(keycloakv1alpha1.ClientScope) bool) (ret []keycloakv1alpha1.ClientScope) {
	for _, s := range mappers {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return ret
}

func filterClients(mappers []keycloakv1alpha1.KeycloakClient, test func(keycloakv1alpha1.KeycloakClient) bool) (ret []keycloakv1alpha1.KeycloakClient) {
	for _, s := range mappers {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return ret
}

func filterUsers(mappers []keycloakv1alpha1.User, test func(keycloakv1alpha1.User) bool) (ret []keycloakv1alpha1.User) {
	for _, s := range mappers {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return ret
}

func filterUserGroups(mappers []keycloakv1alpha1.UserGroup, test func(keycloakv1alpha1.UserGroup) bool) (ret []keycloakv1alpha1.UserGroup) {
	for _, s := range mappers {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return ret
}
