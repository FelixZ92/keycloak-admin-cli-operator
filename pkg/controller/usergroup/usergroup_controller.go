package usergroup

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/Nerzal/gocloak/v4"
	"github.com/go-logr/logr"
	"github.com/operator-framework/operator-sdk/pkg/k8sutil"
	"gitlab.com/felixz92/keycloak-admin-cli-operator/pkg/utils"
	"os"
	"strings"

	keycloakv1alpha1 "gitlab.com/felixz92/keycloak-admin-cli-operator/pkg/apis/keycloak/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("controller_usergroup")

const keycloakUserGroupFinalizer = "finalizer.groups.keycloak.zippelf.com"

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new UserGroup Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	keycloakClient, err := utils.CreateKeycloakClient()
	if err != nil {
		log.Error(err, "Error creating keycloak-client")
	}
	keycloakToken := utils.LoginToKeycloak(keycloakClient)
	return &ReconcileUserGroup{client: mgr.GetClient(), scheme: mgr.GetScheme(), keycloakClient: keycloakClient, keycloakToken: keycloakToken}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("usergroup-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource UserGroup
	err = c.Watch(&source.Kind{Type: &keycloakv1alpha1.UserGroup{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	// external secondary resource (keycloak)

	return nil
}

// blank assignment to verify that ReconcileUserGroup implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileUserGroup{}

// ReconcileUserGroup reconciles a UserGroup object
type ReconcileUserGroup struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client         client.Client
	scheme         *runtime.Scheme
	keycloakClient gocloak.GoCloak
	keycloakToken  *gocloak.JWT
}

// Reconcile reads that state of the cluster for a UserGroup object and makes changes based on the state read
// and what is in the UserGroup.Spec
// Note:
// The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileUserGroup) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := log.WithValues("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Info("Reconciling UserGroup")

	isKeycloakTokenValid := utils.ValidateAccessToken(r.keycloakClient, r.keycloakToken)
	if !isKeycloakTokenValid {
		newToken := utils.RefreshAccessToken(r.keycloakClient, r.keycloakToken)
		if newToken != nil {
			r.keycloakToken = newToken
			reqLogger.Info("Token expired, requeue")
			return reconcile.Result{Requeue: true}, nil
		}
	}

	// Fetch the UserGroup instance
	instance := &keycloakv1alpha1.UserGroup{}
	err := r.client.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	isUserGroupMarkedToBeDeleted := instance.GetDeletionTimestamp() != nil
	if isUserGroupMarkedToBeDeleted {
		return r.finalize(instance, reqLogger)
	}

	// Add finalizer for this CR
	if !utils.Contains(instance.GetFinalizers(), keycloakUserGroupFinalizer) {
		if err := r.addFinalizer(reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}
	}

	// Read the json data
	groupData := gocloak.Group{}
	err = json.Unmarshal([]byte(instance.Spec.Data), &groupData)
	if err != nil {
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	// Fetch the client resource from keycloak
	liveGroup, err := fetchLiveGroup(groupData, r.keycloakClient, r.keycloakToken.AccessToken, instance.Spec.Realm)
	if err != nil {
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	operatorName, found := os.LookupEnv(k8sutil.OperatorNameEnvVar)
	if !found {
		return reconcile.Result{}, fmt.Errorf("%s must be set", k8sutil.OperatorNameEnvVar)
	}

	if liveGroup != nil && instance.Spec.Data != instance.Status.Data {
		// Update it!
		reqLogger.Info("Update client")
		groupData.Attributes["managed-by"] = []string{operatorName}
		groupData.ID = liveGroup.ID
		err := r.keycloakClient.UpdateGroup(r.keycloakToken.AccessToken, instance.Spec.Realm, groupData)
		if err != nil {
			reqLogger.Error(err, "failed to update UserGroup")
			return reconcile.Result{}, err
		}
		instance.Status.GroupID = *liveGroup.ID
		instance.Status.Realm = instance.Spec.Realm
		instance.Status.Data = instance.Spec.Data
		err = r.client.Status().Update(context.TODO(), instance)
		if err != nil {
			reqLogger.Error(err, "failed to update UserGroup")
			return reconcile.Result{}, err
		}
	} else if liveGroup == nil {
		// create it!
		reqLogger.Info("Create UserGroup")
		groupData.Attributes["managed-by"] = []string{operatorName}
		groupID, err := r.keycloakClient.CreateGroup(r.keycloakToken.AccessToken, instance.Spec.Realm, groupData)
		if err != nil {
			reqLogger.Error(err, "failed to create UserGroup")
			return reconcile.Result{}, err
		}

		instance.Status.GroupID = groupID
		instance.Status.Realm = instance.Spec.Realm
		instance.Status.Data = instance.Spec.Data
		err = r.client.Status().Update(context.TODO(), instance)
		if err != nil {
			reqLogger.Error(err, "failed to update UserGroup")
			return reconcile.Result{}, err
		}
	} else {
		reqLogger.Info("not changed")
	}
	return reconcile.Result{}, nil
}

func (r *ReconcileUserGroup) finalize(instance *keycloakv1alpha1.UserGroup, reqLogger logr.Logger) (reconcile.Result, error) {
	if utils.Contains(instance.GetFinalizers(), keycloakUserGroupFinalizer) {
		// Run finalization logic for finalizer. If the
		// finalization logic fails, don't remove the finalizer so
		// that we can retry during the next reconciliation.
		if err := r.finalizeUserGroup(reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}

		// Remove finalizer. Once all finalizers have been
		// removed, the object will be deleted.
		instance.SetFinalizers(utils.Remove(instance.GetFinalizers(), keycloakUserGroupFinalizer))
		err := r.client.Update(context.TODO(), instance)
		if err != nil {
			return reconcile.Result{}, err
		}
	}
	return reconcile.Result{}, nil
}

func (r *ReconcileUserGroup) finalizeUserGroup(reqLogger logr.Logger, group *keycloakv1alpha1.UserGroup) error {
	err := r.keycloakClient.DeleteGroup(r.keycloakToken.AccessToken, group.Spec.Realm, group.Status.GroupID)
	if err != nil {
		if !strings.Contains(err.Error(), "404") {
			// Error reading the object - requeue the request.
			return err
		}
		reqLogger.Error(err, "UserGroup not found")
	}
	reqLogger.Info("Successfully finalized UserGroup")
	return nil
}

func (r *ReconcileUserGroup) addFinalizer(reqLogger logr.Logger, group *keycloakv1alpha1.UserGroup) error {
	reqLogger.Info("Adding Finalizer for the UserGroup")
	group.SetFinalizers(append(group.GetFinalizers(), keycloakUserGroupFinalizer))

	// Update CR
	err := r.client.Update(context.TODO(), group)
	if err != nil {
		reqLogger.Error(err, "Failed to update UserGroup with finalizer")
		return err
	}
	return nil
}

func fetchLiveGroup(groupData gocloak.Group, keycloakClient gocloak.GoCloak, accessToken string, realm string) (*gocloak.Group, error) {
	if groupData.ID != nil {
		liveGroup, err := keycloakClient.GetGroup(accessToken, realm, *groupData.ID)
		if err != nil && !strings.Contains(err.Error(), "404") {
			// Error reading the object - requeue the request.
			return nil, err
		}
		return liveGroup, nil
	} else if groupData.Name != nil {
		liveGroups, err := keycloakClient.GetGroups(accessToken, realm, gocloak.GetGroupsParams{Search: groupData.Name})
		if err != nil {
			// Error reading the object - requeue the request.
			return nil, err
		}
		if len(liveGroups) > 0 {
			return liveGroups[0], nil
		}
	}
	return nil, nil
}
