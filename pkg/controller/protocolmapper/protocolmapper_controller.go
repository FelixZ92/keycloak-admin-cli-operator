package protocolmapper

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/Nerzal/gocloak/v4"
	"github.com/go-logr/logr"
	"github.com/go-resty/resty/v2"
	keycloakv1alpha1 "gitlab.com/felixz92/keycloak-admin-cli-operator/pkg/apis/keycloak/v1alpha1"
	"gitlab.com/felixz92/keycloak-admin-cli-operator/pkg/utils"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
	"strings"
)

var log = logf.Log.WithName("controller_protocolmapper")

const protocolMapperFinalizer = "finalizer.protocolmappers.keycloak.zippelf.com"

// Add creates a new ProtocolMapper Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	keycloakClient, err := utils.CreateKeycloakClient()
	if err != nil {
		log.Error(err, "Error creating keycloak-client")
	}
	keycloakToken := utils.LoginToKeycloak(keycloakClient)
	return &ReconcileProtocolMapper{client: mgr.GetClient(), scheme: mgr.GetScheme(), keycloakClient: keycloakClient, keycloakToken: keycloakToken, restClient: resty.New()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("protocolmapper-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource ProtocolMapper
	err = c.Watch(&source.Kind{Type: &keycloakv1alpha1.ProtocolMapper{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	// external secondary resource (keycloak)

	return nil
}

// blank assignment to verify that ReconcileProtocolMapper implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileProtocolMapper{}

// ReconcileProtocolMapper reconciles a ProtocolMapper object
type ReconcileProtocolMapper struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client         client.Client
	scheme         *runtime.Scheme
	keycloakClient gocloak.GoCloak
	keycloakToken  *gocloak.JWT
	// gocloak does not support to add protocol mappers to a client-scope, so we need to talk to keycloak directly
	restClient *resty.Client
}

// Reconcile reads that state of the cluster for a ProtocolMapper object and makes changes based on the state read
// and what is in the ProtocolMapper.Spec
// Note:
// The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileProtocolMapper) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := log.WithValues("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Info("Reconciling ProtocolMapper")

	isKeycloakTokenValid := utils.ValidateAccessToken(r.keycloakClient, r.keycloakToken)
	if !isKeycloakTokenValid {
		newToken := utils.RefreshAccessToken(r.keycloakClient, r.keycloakToken)
		if newToken != nil {
			r.keycloakToken = newToken
			reqLogger.Info("Token expired, requeue")
			return reconcile.Result{Requeue: true}, nil
		}
	}

	// Fetch the ProtocolMapper instance
	instance := &keycloakv1alpha1.ProtocolMapper{}
	err := r.client.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	isProtocolMapperMarkedToBeDeleted := instance.GetDeletionTimestamp() != nil
	if isProtocolMapperMarkedToBeDeleted {
		return r.finalize(instance, reqLogger)
	}

	// Add finalizer for this CR
	if !utils.Contains(instance.GetFinalizers(), protocolMapperFinalizer) {
		if err := r.addFinalizer(reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}
	}

	// Read the json data
	protocolMapperData := gocloak.ProtocolMappers{}
	err = json.Unmarshal([]byte(instance.Spec.Data), &protocolMapperData)
	if err != nil {
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	clientScopes, err := r.keycloakClient.GetClientScopes(r.keycloakToken.AccessToken, instance.Spec.Realm)

	clientScope := findClientScopeByName(clientScopes, &instance.Spec.ClientScope)
	if clientScope == nil {
		return reconcile.Result{}, fmt.Errorf("client scope %s not found", instance.Spec.ClientScope)
	}

	liveProtocolMapper := findProtocolMapperByName(clientScope.ProtocolMappers, protocolMapperData.Name)

	if liveProtocolMapper != nil && instance.Spec.Data != instance.Status.Data {
		// Delete mapper, then trigger reconciliation to recreate it
		err = r.finalizeProtocolMapper(reqLogger, instance)
		if err != nil {
			reqLogger.Error(err, "failed to update client scope")
			return reconcile.Result{}, err
		}
		return reconcile.Result{Requeue: true}, nil
	} else if liveProtocolMapper == nil || instance.Status.ClientScopeID == "" {
		// Create it!!
		reqLogger.Info("Create ProtocolMapper")

		endpoint, err := utils.GetKeycloakEndpoint()
		resp, err := r.restClient.R().EnableTrace().
			SetAuthToken(r.keycloakToken.AccessToken).
			SetHeader("Accept", "application/json").
			SetHeader("Content-Type", "application/json").
			SetPathParams(map[string]string{
				"realm":           instance.Spec.Realm,
				"client_scope_id": *clientScope.ID,
			}).
			SetBody(instance.Spec.Data).
			Post(endpoint + "auth/admin/realms/{realm}/client-scopes/{client_scope_id}/protocol-mappers/models")
		if err != nil {
			return reconcile.Result{}, fmt.Errorf("could not create mapper %s", instance.Name)
		}

		protocolMapperID := strings.Split(resp.Header().Get("Location"), "models/")[1]

		instance.Status.ClientScopeID = *clientScope.ID
		instance.Status.Realm = instance.Spec.Realm
		instance.Status.ProtocolMapperID = protocolMapperID
		instance.Status.Data = instance.Spec.Data
		err = r.client.Status().Update(context.TODO(), instance)
		if err != nil {
			reqLogger.Error(err, "failed to update ProtocolMapper")
			return reconcile.Result{}, err
		}

		debugRequest(err, resp)
	} else {
		reqLogger.Info("not changed")
	}
	return reconcile.Result{}, nil
}

func (r *ReconcileProtocolMapper) finalize(instance *keycloakv1alpha1.ProtocolMapper, reqLogger logr.Logger) (reconcile.Result, error) {
	if utils.Contains(instance.GetFinalizers(), protocolMapperFinalizer) {
		// Run finalization logic for finalizer. If the
		// finalization logic fails, don't remove the finalizer so
		// that we can retry during the next reconciliation.
		if err := r.finalizeProtocolMapper(reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}

		// Remove finalizer. Once all finalizers have been
		// removed, the object will be deleted.
		instance.SetFinalizers(utils.Remove(instance.GetFinalizers(), protocolMapperFinalizer))
		err := r.client.Update(context.TODO(), instance)
		if err != nil {
			return reconcile.Result{}, err
		}
	}
	return reconcile.Result{}, nil
}

func (r *ReconcileProtocolMapper) finalizeProtocolMapper(reqLogger logr.Logger, kc *keycloakv1alpha1.ProtocolMapper) error {
	// Delete mapper
	reqLogger.Info("Delete protocol mapper")

	endpoint, err := utils.GetKeycloakEndpoint()
	if err != nil {
		reqLogger.Error(err, "delete protocol mapper - keycloak endpoint not set")
		return err
	}
	resp, err := r.restClient.R().EnableTrace().
		SetAuthToken(r.keycloakToken.AccessToken).
		SetHeader("Accept", "application/json").
		SetHeader("Content-Type", "application/json").
		SetPathParams(map[string]string{
			"realm":           kc.Spec.Realm,
			"client_scope_id": kc.Status.ClientScopeID,
			"mapperId": kc.Status.ProtocolMapperID,
		}).
		SetBody(kc.Spec.Data).
		Delete(endpoint + "auth/admin/realms/{realm}/client-scopes/{client_scope_id}/protocol-mappers/models/{mapperId}")
	if err != nil {
		reqLogger.Error(err, "delete protocol mapper - failed to delete protocolmapper")
	}

	debugRequest(err, resp)
	reqLogger.Info("Successfully finalized protocol mapper")
	return nil
}

func (r *ReconcileProtocolMapper) addFinalizer(reqLogger logr.Logger, kc *keycloakv1alpha1.ProtocolMapper) error {
	reqLogger.Info("Adding Finalizer for the ProtocolMapper")
	kc.SetFinalizers(append(kc.GetFinalizers(), protocolMapperFinalizer))

	// Update CR
	err := r.client.Update(context.TODO(), kc)
	if err != nil {
		reqLogger.Error(err, "Failed to update ProtocolMapper with finalizer")
		return err
	}
	return nil
}

func findClientScopeByName(clientScopes []*gocloak.ClientScope, clientScopeName *string) *gocloak.ClientScope {
	for _, cs := range clientScopes {
		if *cs.Name == *clientScopeName {
			return cs
		}
	}
	return nil
}

func findProtocolMapperByName(protocolMappers []*gocloak.ProtocolMappers, protocolMapperName *string) *gocloak.ProtocolMappers {
	for _, pm := range protocolMappers {
		if *pm.Name == *protocolMapperName {
			return pm
		}
	}
	return nil
}

func remove(list []*gocloak.ProtocolMappers, s *gocloak.ProtocolMappers) []*gocloak.ProtocolMappers {
	for i, v := range list {
		if v == s {
			list = append(list[:i], list[i+1:]...)
		}
	}
	return list
}

func debugRequest(err error, resp *resty.Response) {
	// Explore response object
	fmt.Println("Response Info:")
	fmt.Println("Error      :", err)
	fmt.Println("Status Code:", resp.StatusCode())
	fmt.Println("Status     :", resp.Status())
	fmt.Println("Time       :", resp.Time())
	fmt.Println("Received At:", resp.ReceivedAt())
	fmt.Println("Body       :\n", resp)
	fmt.Println("request:", resp.Request.URL)
	fmt.Println("body:", resp.Request.Body)
	fmt.Println("header:", resp.Header())
	fmt.Println()

	// Explore trace info
	fmt.Println("Request Trace Info:")
	ti := resp.Request.TraceInfo()
	fmt.Println("DNSLookup    :", ti.DNSLookup)
	fmt.Println("ConnTime     :", ti.ConnTime)
	fmt.Println("TLSHandshake :", ti.TLSHandshake)
	fmt.Println("ServerTime   :", ti.ServerTime)
	fmt.Println("ResponseTime :", ti.ResponseTime)
	fmt.Println("TotalTime    :", ti.TotalTime)
	fmt.Println("IsConnReused :", ti.IsConnReused)
	fmt.Println("IsConnWasIdle:", ti.IsConnWasIdle)
	fmt.Println("ConnIdleTime :", ti.ConnIdleTime)
}
