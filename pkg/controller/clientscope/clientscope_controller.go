package clientscope

import (
	"context"
	"encoding/json"
	"github.com/Nerzal/gocloak/v4"
	"github.com/go-logr/logr"
	"github.com/go-resty/resty/v2"
	"gitlab.com/felixz92/keycloak-admin-cli-operator/pkg/utils"
	"strings"

	keycloakv1alpha1 "gitlab.com/felixz92/keycloak-admin-cli-operator/pkg/apis/keycloak/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("controller_clientscope")

const keycloakClientScopeFinalizer = "finalizer.clientScopes.keycloak.zippelf.com"

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new ClientScope Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	keycloakClient, err := utils.CreateKeycloakClient()
	if err != nil {
		log.Error(err, "Error creating keycloak-client")
	}
	keycloakToken := utils.LoginToKeycloak(keycloakClient)
	return &ReconcileClientScope{client: mgr.GetClient(), scheme: mgr.GetScheme(), keycloakClient: keycloakClient, keycloakToken: keycloakToken, restClient: resty.New()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("clientscope-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource ClientScope
	err = c.Watch(&source.Kind{Type: &keycloakv1alpha1.ClientScope{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	// external secondary resource (keycloak)

	return nil
}

// blank assignment to verify that ReconcileClientScope implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileClientScope{}

// ReconcileClientScope reconciles a ClientScope object
type ReconcileClientScope struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client         client.Client
	scheme         *runtime.Scheme
	keycloakClient gocloak.GoCloak
	keycloakToken  *gocloak.JWT
	restClient *resty.Client
}

// Reconcile reads that state of the cluster for a ClientScope object and makes changes based on the state read
// and what is in the ClientScope.Spec
// Note:
// The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileClientScope) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := log.WithValues("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Info("Reconciling ClientScope")

	isKeycloakTokenValid := utils.ValidateAccessToken(r.keycloakClient, r.keycloakToken)
	if !isKeycloakTokenValid {
		newToken := utils.RefreshAccessToken(r.keycloakClient, r.keycloakToken)
		if newToken != nil {
			r.keycloakToken = newToken
			reqLogger.Info("Token expired, requeue")
			return reconcile.Result{Requeue: true}, nil
		}
	}

	// Fetch the ClientScope instance
	instance := &keycloakv1alpha1.ClientScope{}
	err := r.client.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	isClientScopeMarkedToBeDeleted := instance.GetDeletionTimestamp() != nil
	if isClientScopeMarkedToBeDeleted {
		return r.finalize(instance, reqLogger)
	}

	// Add finalizer for this CR
	if !utils.Contains(instance.GetFinalizers(), keycloakClientScopeFinalizer) {
		if err := r.addFinalizer(reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}
	}

	// Read the json data
	clientScopeData := gocloak.ClientScope{}
	err = json.Unmarshal([]byte(instance.Spec.Data), &clientScopeData)
	if err != nil {
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	clientScopes, err := r.keycloakClient.GetClientScopes(r.keycloakToken.AccessToken, instance.Spec.Realm)
	if err != nil {
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	liveClientScope := findClientScopeByName(clientScopes, clientScopeData.Name)
	if liveClientScope != nil && instance.Spec.Data != instance.Status.Data {
		// Update
		// Delete scope, then trigger reconciliation to recreate it
		reqLogger.Info("Update client")
		err = r.finalizeClientScope(reqLogger, instance)
		if err != nil {
			reqLogger.Error(err, "failed to delete client scope")
			return reconcile.Result{}, err
		}

		mappers := &keycloakv1alpha1.ProtocolMapperList{}
		err = r.client.List(context.TODO(), mappers)
		if err != nil {
			reqLogger.Error(err, "failed to get ProtocolMapper")
			return reconcile.Result{}, err
		}
		filter := func(mapper keycloakv1alpha1.ProtocolMapper) bool { return mapper.Spec.ClientScope == *clientScopeData.Name }
		relevantMappers := filterProtocolMapper(mappers.Items, filter)

		for _, mapper := range relevantMappers {
			mapper.Status.ClientScopeID = ""
			err = r.client.Status().Update(context.TODO(), &mapper)
			if err != nil {
				reqLogger.Error(err, "failed to update ProtocolMapper")
				return reconcile.Result{}, err
			}
		}

		return reconcile.Result{Requeue: true}, nil
	} else if liveClientScope == nil {
		// create
		reqLogger.Info("Create clientScope")
		clientScopeID, err := r.keycloakClient.CreateClientScope(r.keycloakToken.AccessToken, instance.Spec.Realm, clientScopeData)
		if err != nil {
			reqLogger.Error(err, "failed to create clientScope")
			return reconcile.Result{}, err
		}

		instance.Status.ClientScopeID = clientScopeID
		instance.Status.Realm = instance.Spec.Realm
		instance.Status.Data = instance.Spec.Data
		err = r.client.Status().Update(context.TODO(), instance)
		if err != nil {
			reqLogger.Error(err, "failed to update clientScope")
			return reconcile.Result{}, err
		}
	} else {
		reqLogger.Info("not changed")
	}
	return reconcile.Result{}, nil
}

func (r *ReconcileClientScope) finalize(instance *keycloakv1alpha1.ClientScope, reqLogger logr.Logger) (reconcile.Result, error) {
	if utils.Contains(instance.GetFinalizers(), keycloakClientScopeFinalizer) {
		// Run finalization logic for finalizer. If the
		// finalization logic fails, don't remove the finalizer so
		// that we can retry during the next reconciliation.
		if err := r.finalizeClientScope(reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}

		// Remove finalizer. Once all finalizers have been
		// removed, the object will be deleted.
		instance.SetFinalizers(utils.Remove(instance.GetFinalizers(), keycloakClientScopeFinalizer))
		err := r.client.Update(context.TODO(), instance)
		if err != nil {
			return reconcile.Result{}, err
		}
	}
	return reconcile.Result{}, nil
}

func (r *ReconcileClientScope) finalizeClientScope(reqLogger logr.Logger, kc *keycloakv1alpha1.ClientScope) error {

	err := r.keycloakClient.DeleteClientScope(r.keycloakToken.AccessToken, kc.Spec.Realm, kc.Status.ClientScopeID)
	if err != nil {
		if !strings.Contains(err.Error(), "404") {
			// Error reading the object - requeue the request.
			return err
		}
		reqLogger.Error(err, "clientScope not found")
	}
	reqLogger.Info("Successfully finalized clientScope")
	return nil
}

func (r *ReconcileClientScope) addFinalizer(reqLogger logr.Logger, kc *keycloakv1alpha1.ClientScope) error {
	reqLogger.Info("Adding Finalizer for the ClientScope")
	kc.SetFinalizers(append(kc.GetFinalizers(), keycloakClientScopeFinalizer))

	// Update CR
	err := r.client.Update(context.TODO(), kc)
	if err != nil {
		reqLogger.Error(err, "Failed to update ClientScope with finalizer")
		return err
	}
	return nil
}

func findClientScopeByName(clientScopes []*gocloak.ClientScope, clientScopeName *string) *gocloak.ClientScope {
	for _, cs := range clientScopes {
		if *cs.Name == *clientScopeName {
			return cs
		}
	}
	return nil
}

func filterProtocolMapper(mappers []keycloakv1alpha1.ProtocolMapper, test func(keycloakv1alpha1.ProtocolMapper) bool) (ret []keycloakv1alpha1.ProtocolMapper) {
	for _, s := range mappers {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return
}
