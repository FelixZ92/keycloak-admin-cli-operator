package user

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/Nerzal/gocloak/v4"
	"github.com/go-logr/logr"
	"github.com/operator-framework/operator-sdk/pkg/k8sutil"
	"gitlab.com/felixz92/keycloak-admin-cli-operator/pkg/utils"
	"os"
	"reflect"
	"sort"
	"strings"

	keycloakv1alpha1 "gitlab.com/felixz92/keycloak-admin-cli-operator/pkg/apis/keycloak/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("controller_user")

const keycloakUserFinalizer = "finalizer.users.keycloak.zippelf.com"

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new User Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	keycloakClient, err := utils.CreateKeycloakClient()
	if err != nil {
		log.Error(err, "Error creating keycloak-client")
	}
	keycloakToken := utils.LoginToKeycloak(keycloakClient)
	return &ReconcileUser{client: mgr.GetClient(), scheme: mgr.GetScheme(), keycloakClient: keycloakClient, keycloakToken: keycloakToken}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("user-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource User
	err = c.Watch(&source.Kind{Type: &keycloakv1alpha1.User{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	// external secondary resource (keycloak)

	return nil
}

// blank assignment to verify that ReconcileUser implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileUser{}

// ReconcileUser reconciles a User object
type ReconcileUser struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client         client.Client
	scheme         *runtime.Scheme
	keycloakClient gocloak.GoCloak
	keycloakToken  *gocloak.JWT
}

// Reconcile reads that state of the cluster for a User object and makes changes based on the state read
// and what is in the User.Spec
// TODO(user): Modify this Reconcile function to implement your Controller logic.  This example creates
// a Pod as an example
// Note:
// The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileUser) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := log.WithValues("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Info("Reconciling User")

	isKeycloakTokenValid := utils.ValidateAccessToken(r.keycloakClient, r.keycloakToken)
	if !isKeycloakTokenValid {
		newToken := utils.RefreshAccessToken(r.keycloakClient, r.keycloakToken)
		if newToken != nil {
			r.keycloakToken = newToken
			reqLogger.Info("Token expired, requeue")
			return reconcile.Result{Requeue: true}, nil
		}
	}

	// Fetch the User instance
	instance := &keycloakv1alpha1.User{}
	err := r.client.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	isUserMarkedToBeDeleted := instance.GetDeletionTimestamp() != nil
	if isUserMarkedToBeDeleted {
		return r.finalize(instance, reqLogger)
	}

	// Add finalizer for this CR
	if !utils.Contains(instance.GetFinalizers(), keycloakUserFinalizer) {
		if err := r.addFinalizer(reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}
	}

	// Read the json data
	userData := gocloak.User{}
	err = json.Unmarshal([]byte(instance.Spec.Data), &userData)
	if err != nil {
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	// Fetch the user resource from keycloak
	liveUser, err := fetchLiveUser(userData, r.keycloakClient, r.keycloakToken.AccessToken, instance.Spec.Realm)
	if err != nil {
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	operatorName, found := os.LookupEnv(k8sutil.OperatorNameEnvVar)
	if !found {
		return reconcile.Result{}, fmt.Errorf("%s must be set", k8sutil.OperatorNameEnvVar)
	}
	if liveUser != nil && (instance.Spec.Data != instance.Status.Data || !arrayEquals(instance.Spec.Groups, instance.Status.Groups)) {
		// Update it!
		reqLogger.Info("Update User")
		userData.Attributes["managed-by"] = []string{operatorName}
		userData.ID = liveUser.ID
		err := r.keycloakClient.UpdateUser(r.keycloakToken.AccessToken, instance.Spec.Realm, userData)
		if err != nil {
			reqLogger.Error(err, "failed to update User")
			return reconcile.Result{}, err
		}

		for _, group := range instance.Status.Groups {
			groupID, err := fetchGroupID(group, r.keycloakClient, r.keycloakToken.AccessToken, instance.Spec.Realm)
			if err != nil {
				reqLogger.Error(err, "Group not found")
				return reconcile.Result{}, err
			}
			if groupID != nil {
				err = r.keycloakClient.DeleteUserFromGroup(r.keycloakToken.AccessToken, instance.Spec.Realm, *liveUser.ID, *groupID)
				if err != nil {
					reqLogger.Error(err, "Could not remove user from group")
					return reconcile.Result{}, err
				}
			}
		}

		for _, group := range instance.Spec.Groups {
			groupID, err := fetchGroupID(group, r.keycloakClient, r.keycloakToken.AccessToken, instance.Spec.Realm)
			if err != nil {
				reqLogger.Error(err, "Group not found")
				return reconcile.Result{}, err
			}
			if groupID != nil {
				err = r.keycloakClient.AddUserToGroup(r.keycloakToken.AccessToken, instance.Spec.Realm, *liveUser.ID, *groupID)
				if err != nil {
					reqLogger.Error(err, "Could not add user to group")
					return reconcile.Result{}, err
				}
			}
		}

		instance.Status.UserID = *liveUser.ID
		instance.Status.Realm = instance.Spec.Realm
		instance.Status.Data = instance.Spec.Data
		instance.Status.Groups = instance.Spec.Groups
		err = r.client.Status().Update(context.TODO(), instance)
		if err != nil {
			reqLogger.Error(err, "failed to update User")
			return reconcile.Result{}, err
		}
	} else if liveUser == nil {
		// create it!
		reqLogger.Info("Create User")
		userData.Attributes["managed-by"] = []string{operatorName}
		userID, err := r.keycloakClient.CreateUser(r.keycloakToken.AccessToken, instance.Spec.Realm, userData)
		if err != nil {
			reqLogger.Error(err, "failed to create User")
			return reconcile.Result{}, err
		}
		for _, group := range instance.Spec.Groups {
			groupID, err := fetchGroupID(group, r.keycloakClient, r.keycloakToken.AccessToken, instance.Spec.Realm)
			if err != nil {
				reqLogger.Error(err, "Group not found")
				return reconcile.Result{}, err
			}
			if groupID != nil {
				err = r.keycloakClient.AddUserToGroup(r.keycloakToken.AccessToken, instance.Spec.Realm, userID, *groupID)
				if err != nil {
					reqLogger.Error(err, "Could not add user to group")
					return reconcile.Result{}, err
				}
			}
		}

		instance.Status.UserID = userID
		instance.Status.Realm = instance.Spec.Realm
		instance.Status.Data = instance.Spec.Data
		instance.Status.Groups = instance.Spec.Groups
		err = r.client.Status().Update(context.TODO(), instance)
		if err != nil {
			reqLogger.Error(err, "failed to update User")
			return reconcile.Result{}, err
		}
	} else {
		reqLogger.Info("not changed")
	}
	return reconcile.Result{}, nil
}

func arrayEquals(groups []string, groups2 []string) bool {
	a := groups
	b := groups2
	sort.Strings(a)
	sort.Strings(b)
	return reflect.DeepEqual(a, b)
}

func (r *ReconcileUser) finalize(instance *keycloakv1alpha1.User, reqLogger logr.Logger) (reconcile.Result, error) {
	if utils.Contains(instance.GetFinalizers(), keycloakUserFinalizer) {
		// Run finalization logic for finalizer. If the
		// finalization logic fails, don't remove the finalizer so
		// that we can retry during the next reconciliation.
		if err := r.finalizeUser(reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}

		// Remove finalizer. Once all finalizers have been
		// removed, the object will be deleted.
		instance.SetFinalizers(utils.Remove(instance.GetFinalizers(), keycloakUserFinalizer))
		err := r.client.Update(context.TODO(), instance)
		if err != nil {
			return reconcile.Result{}, err
		}
	}
	return reconcile.Result{}, nil
}

func (r *ReconcileUser) finalizeUser(reqLogger logr.Logger, user *keycloakv1alpha1.User) error {
	err := r.keycloakClient.DeleteUser(r.keycloakToken.AccessToken, user.Spec.Realm, user.Status.UserID)
	if err != nil {
		if !strings.Contains(err.Error(), "404") {
			// Error reading the object - requeue the request.
			return err
		}
		reqLogger.Error(err, "User not found")
	}
	reqLogger.Info("Successfully finalized User")
	return nil
}

func (r *ReconcileUser) addFinalizer(reqLogger logr.Logger, user *keycloakv1alpha1.User) error {
	reqLogger.Info("Adding Finalizer for the User")
	user.SetFinalizers(append(user.GetFinalizers(), keycloakUserFinalizer))

	// Update CR
	err := r.client.Update(context.TODO(), user)
	if err != nil {
		reqLogger.Error(err, "Failed to update User with finalizer")
		return err
	}
	return nil
}

func fetchLiveUser(userData gocloak.User, keycloakClient gocloak.GoCloak, accessToken string, realm string) (*gocloak.User, error) {
	if userData.ID != nil {
		liveUser, err := keycloakClient.GetUserByID(accessToken, realm, *userData.ID)
		if err != nil && !strings.Contains(err.Error(), "404") {
			// Error reading the object - requeue the request.
			return nil, err
		}
		return liveUser, nil
	} else if userData.Username != nil {
		liveUsers, err := keycloakClient.GetUsers(accessToken, realm, gocloak.GetUsersParams{Search: userData.Username})
		if err != nil {
			// Error reading the object - requeue the request.
			return nil, err
		}
		if len(liveUsers) > 0 {
			return liveUsers[0], nil
		}
	}
	return nil, nil
}

func fetchGroupID(name string, keycloakClient gocloak.GoCloak, accessToken string, realm string) (*string, error) {
	liveGroups, err := keycloakClient.GetGroups(accessToken, realm, gocloak.GetGroupsParams{Search: &name})
	if err != nil {
		return nil, err
	}
	if len(liveGroups) > 0 {
		return liveGroups[0].ID, nil
	}
	return nil, nil
}
