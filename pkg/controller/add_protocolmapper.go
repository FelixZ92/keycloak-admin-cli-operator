package controller

import (
	"gitlab.com/felixz92/keycloak-admin-cli-operator/pkg/controller/protocolmapper"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, protocolmapper.Add)
}
