package keycloakclient

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/Nerzal/gocloak/v4"
	"github.com/go-logr/logr"
	"github.com/operator-framework/operator-sdk/pkg/k8sutil"
	keycloakv1alpha1 "gitlab.com/felixz92/keycloak-admin-cli-operator/pkg/apis/keycloak/v1alpha1"
	"gitlab.com/felixz92/keycloak-admin-cli-operator/pkg/utils"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"os"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
	"strings"
)

var log = logf.Log.WithName("controller_keycloakclient")

const keycloakClientFinalizer = "finalizer.keycloakclients.keycloak.zippelf.com"

// Add creates a new KeycloakClient Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	keycloakClient, err := utils.CreateKeycloakClient()
	if err != nil {
		log.Error(err, "Error creating keycloak-client")
	}
	keycloakToken := utils.LoginToKeycloak(keycloakClient)
	return &ReconcileKeycloakClient{client: mgr.GetClient(), scheme: mgr.GetScheme(), keycloakClient: keycloakClient, keycloakToken: keycloakToken}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("keycloakclient-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource KeycloakClient
	err = c.Watch(&source.Kind{Type: &keycloakv1alpha1.KeycloakClient{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	// external secondary resource (keycloak)

	return nil
}

// blank assignment to verify that ReconcileKeycloakClient implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileKeycloakClient{}

// ReconcileKeycloakClient reconciles a KeycloakClient object
type ReconcileKeycloakClient struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client         client.Client
	scheme         *runtime.Scheme
	keycloakClient gocloak.GoCloak
	keycloakToken  *gocloak.JWT
}

// Reconcile reads that state of the cluster for a KeycloakClient object and makes changes based on the state read
// and what is in the KeycloakClient.Spec
// Note:
// The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileKeycloakClient) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := log.WithValues("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Info("Reconciling KeycloakClient")

	isKeycloakTokenValid := utils.ValidateAccessToken(r.keycloakClient, r.keycloakToken)
	if !isKeycloakTokenValid {
		newToken := utils.RefreshAccessToken(r.keycloakClient, r.keycloakToken)
		if newToken != nil {
			r.keycloakToken = newToken
			reqLogger.Info("Token expired, requeue")
			return reconcile.Result{Requeue: true}, nil
		}
	}

	// Fetch the KeycloakClient instance
	instance := &keycloakv1alpha1.KeycloakClient{}
	err := r.client.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	isKeycloakClientMarkedToBeDeleted := instance.GetDeletionTimestamp() != nil
	if isKeycloakClientMarkedToBeDeleted {
		return r.finalize(instance, reqLogger)
	}

	// Add finalizer for this CR
	if !utils.Contains(instance.GetFinalizers(), keycloakClientFinalizer) {
		if err := r.addFinalizer(reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}
	}

	// Read the json data
	clientData := gocloak.Client{}
	err = json.Unmarshal([]byte(instance.Spec.Data), &clientData)
	if err != nil {
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	// Fetch the client resource from keycloak
	liveClient, err := fetchLiveClient(clientData, r.keycloakClient, r.keycloakToken.AccessToken, instance.Spec.Realm)
	if err != nil {
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	operatorName, found := os.LookupEnv(k8sutil.OperatorNameEnvVar)
	if !found {
		return reconcile.Result{}, fmt.Errorf("%s must be set", k8sutil.OperatorNameEnvVar)
	}

	if liveClient != nil && instance.Spec.Data != instance.Status.Data {
		// Update it!
		reqLogger.Info("Update client")
		clientData.Attributes["managed-by"] = operatorName
		clientData.ID = liveClient.ID
		err := r.keycloakClient.UpdateClient(r.keycloakToken.AccessToken, instance.Spec.Realm, clientData)
		if err != nil {
			reqLogger.Error(err, "failed to update client")
			return reconcile.Result{}, err
		}
		instance.Status.ClientID = *liveClient.ID
		instance.Status.Realm = instance.Spec.Realm
		instance.Status.Data = instance.Spec.Data
		err = r.client.Status().Update(context.TODO(), instance)
		if err != nil {
			reqLogger.Error(err, "failed to update KeycloakClient")
			return reconcile.Result{}, err
		}
		if *clientData.ClientAuthenticatorType == "client-secret" {
			// try to fetch the secret
			result, err := r.createSecret(instance, instance.Status.ClientID, reqLogger)
			if err != nil {
				return result, err
			}
		}
	} else if liveClient == nil {
		// create it!
		reqLogger.Info("Create client")
		clientData.Attributes["managed-by"] = operatorName
		clientID, err := r.keycloakClient.CreateClient(r.keycloakToken.AccessToken, instance.Spec.Realm, clientData)
		if err != nil {
			reqLogger.Error(err, "failed to create client")
			return reconcile.Result{}, err
		}

		instance.Status.ClientID = clientID
		instance.Status.Realm = instance.Spec.Realm
		instance.Status.Data = instance.Spec.Data
		err = r.client.Status().Update(context.TODO(), instance)
		if err != nil {
			reqLogger.Error(err, "failed to update KeycloakClient")
			return reconcile.Result{}, err
		}

		if *clientData.ClientAuthenticatorType == "client-secret" {
			result, err := r.createSecret(instance, clientID, reqLogger)
			if err != nil {
				return result, err
			}
		}
	} else {
		reqLogger.Info("not changed")
	}
	return reconcile.Result{}, nil
}

func (r *ReconcileKeycloakClient) createSecret(instance *keycloakv1alpha1.KeycloakClient, clientID string, reqLogger logr.Logger) (reconcile.Result, error) {
	clientSecret, err := r.keycloakClient.GetClientSecret(r.keycloakToken.AccessToken, instance.Spec.Realm, clientID)
	if err != nil {
		return reconcile.Result{}, err
	}

	secret := &corev1.Secret{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Namespace: instance.Namespace, Name: instance.Name + "-secret"}, secret)
	if err != nil {
		reqLogger.Info("Secret not found, creating it")
		secret := newSecretForCR(instance, *clientSecret.Value)
		if err := controllerutil.SetControllerReference(instance, secret, r.scheme); err != nil {
			reqLogger.Error(err, "unable to set owner reference on new secret")
			return reconcile.Result{}, err
		}
		if err := r.client.Create(context.TODO(), secret); err != nil {
			reqLogger.Error(err, "failed to create secret")
			return reconcile.Result{}, err
		}
	} else {
		reqLogger.Info("updating secret")
		stringData := map[string]string{
			"clientSecret": *clientSecret.Value,
		}
		secret.StringData = stringData
		if err := r.client.Update(context.TODO(), secret); err != nil {
			reqLogger.Error(err, "failed to update secret")
			return reconcile.Result{}, err
		}
	}

	return reconcile.Result{}, nil
}

func (r *ReconcileKeycloakClient) finalize(instance *keycloakv1alpha1.KeycloakClient, reqLogger logr.Logger) (reconcile.Result, error) {
	if utils.Contains(instance.GetFinalizers(), keycloakClientFinalizer) {
		// Run finalization logic for finalizer. If the
		// finalization logic fails, don't remove the finalizer so
		// that we can retry during the next reconciliation.
		if err := r.finalizeKeycloakClient(reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}

		// Remove finalizer. Once all finalizers have been
		// removed, the object will be deleted.
		instance.SetFinalizers(utils.Remove(instance.GetFinalizers(), keycloakClientFinalizer))
		err := r.client.Update(context.TODO(), instance)
		if err != nil {
			return reconcile.Result{}, err
		}
	}
	return reconcile.Result{}, nil
}

func (r *ReconcileKeycloakClient) finalizeKeycloakClient(reqLogger logr.Logger, kc *keycloakv1alpha1.KeycloakClient) error {
	err := r.keycloakClient.DeleteClient(r.keycloakToken.AccessToken, kc.Spec.Realm, kc.Status.ClientID)
	if err != nil {
		if !strings.Contains(err.Error(), "404") {
			// Error reading the object - requeue the request.
			return err
		}
		reqLogger.Error(err, "client not found")
	}
	reqLogger.Info("Successfully finalized memcached")
	return nil
}

func (r *ReconcileKeycloakClient) addFinalizer(reqLogger logr.Logger, kc *keycloakv1alpha1.KeycloakClient) error {
	reqLogger.Info("Adding Finalizer for the KeycloakClient")
	kc.SetFinalizers(append(kc.GetFinalizers(), keycloakClientFinalizer))

	// Update CR
	err := r.client.Update(context.TODO(), kc)
	if err != nil {
		reqLogger.Error(err, "Failed to update KeycloakClient with finalizer")
		return err
	}
	return nil
}

func fetchLiveClient(clientData gocloak.Client, keycloakClient gocloak.GoCloak, accessToken string, realm string) (*gocloak.Client, error) {
	if clientData.ID != nil {
		liveClient, err := keycloakClient.GetClient(accessToken, realm, *clientData.ID)
		if err != nil && !strings.Contains(err.Error(), "404") {
			// Error reading the object - requeue the request.
			return nil, err
		}
		return liveClient, nil
	} else if clientData.ClientID != nil {
		liveClients, err := keycloakClient.GetClients(accessToken, realm, gocloak.GetClientsParams{ClientID: clientData.ClientID})
		if err != nil {
			// Error reading the object - requeue the request.
			return nil, err
		}
		if len(liveClients) > 0 {
			return liveClients[0], nil
		}
	}
	return nil, nil
}

func newSecretForCR(cr *keycloakv1alpha1.KeycloakClient, clientSecret string) *corev1.Secret {
	labels := map[string]string{
		"keycloakclients.keycloak.zippelf.com/name": cr.Name,
	}
	stringData := map[string]string{
		"clientSecret": clientSecret,
	}
	return &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Name + "-secret",
			Namespace: cr.Namespace,
			Labels:    labels,
		},
		StringData: stringData,
	}
}
