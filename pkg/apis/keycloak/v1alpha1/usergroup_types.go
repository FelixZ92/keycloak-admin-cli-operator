package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// UserGroupSpec defines the desired state of UserGroup
type UserGroupSpec struct {
	Realm string `json:"realm"`
	Data  string `json:"data"`
}

// UserGroupStatus defines the observed state of UserGroup
type UserGroupStatus struct {
	Realm   string `json:"realm"`
	GroupID string `json:"groupId"`
	Data    string `json:"data"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// UserGroup is the Schema for the usergroups API
// +kubebuilder:subresource:status
// +kubebuilder:resource:path=usergroups,scope=Namespaced
type UserGroup struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   UserGroupSpec   `json:"spec,omitempty"`
	Status UserGroupStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// UserGroupList contains a list of UserGroup
type UserGroupList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []UserGroup `json:"items"`
}

func init() {
	SchemeBuilder.Register(&UserGroup{}, &UserGroupList{})
}
