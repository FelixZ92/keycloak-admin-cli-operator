package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// ClientScopeSpec defines the desired state of ClientScope
type ClientScopeSpec struct {
	Realm string `json:"realm"`
	Data  string `json:"data"`
}

// ClientScopeStatus defines the observed state of ClientScope
type ClientScopeStatus struct {
	Realm         string `json:"realm"`
	ClientScopeID string `json:"clientScopeId"`
	Data          string `json:"data"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClientScope is the Schema for the clientscopes API
// +kubebuilder:subresource:status
// +kubebuilder:resource:path=clientscopes,scope=Namespaced
type ClientScope struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ClientScopeSpec   `json:"spec,omitempty"`
	Status ClientScopeStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClientScopeList contains a list of ClientScope
type ClientScopeList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ClientScope `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ClientScope{}, &ClientScopeList{})
}
