package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ProtocolMapperSpec defines the desired state of ProtocolMapper
type ProtocolMapperSpec struct {
	Realm string `json:"realm"`
	ClientScope string `json:"clientScope"`
	Data  string `json:"data"`
}

// ProtocolMapperStatus defines the observed state of ProtocolMapper
type ProtocolMapperStatus struct {
	Realm            string `json:"realm"`
	ProtocolMapperID string `json:"protocolMapperId"`
	ClientScopeID    string `json:"clientScopeId"`
	Data             string `json:"data"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProtocolMapper is the Schema for the protocolmappers APIs
// +kubebuilder:subresource:status
// +kubebuilder:resource:path=protocolmappers,scope=Namespaced
type ProtocolMapper struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ProtocolMapperSpec   `json:"spec,omitempty"`
	Status ProtocolMapperStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProtocolMapperList contains a list of ProtocolMapper
type ProtocolMapperList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ProtocolMapper `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ProtocolMapper{}, &ProtocolMapperList{})
}
